from django.db import models
from tasks.models import Project, Task


class BugReport(models.Model):
    STATUS_CHOICES = [
        ('New', 'Новая'),
        ('In_progress', 'В работе'),
        ('Completed', 'Завершена'),
    ]
    PRIORITY_CHOICES = [
        (1, '1'),
        (2, '2'),
        (3, '3'),
        (4, '4'),
        (5, '5'),
    ]
    title = models.CharField(max_length=100, verbose_name='Краткое описание бага')
    description = models.TextField(verbose_name='Полное описание бага')
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    task = models.ForeignKey(Task, on_delete=models.SET_NULL, null=True, blank=True)
    status = models.CharField(
        max_length=50,
        choices=STATUS_CHOICES,
        default='New',
        verbose_name='Статус бага'
    )
    priority = models.SmallIntegerField(
        choices=PRIORITY_CHOICES,
        verbose_name = 'Приоритет бага'
    )
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class FeatureRequest(models.Model):
    STATUS_CHOICES = [
        ('consideration', 'Рассмотрение'),
        ('accepted', 'Принято'),
        ('rejection', 'Отклонение'),
    ]
    PRIORITY_CHOICES = [
        (1, '1'),
        (2, '2'),
        (3, '3'),
        (4, '4'),
        (5, '5'),
    ]
    title = models.CharField(max_length=50, verbose_name='Название запроса на новую функцию')
    description = models.TextField(verbose_name='Описание запроса')
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    task = models.ForeignKey(Task, on_delete=models.SET_NULL, null=True, blank=True)
    status = models.CharField(
        max_length=50,
        choices=STATUS_CHOICES,
        default='New',
        verbose_name='Статус запроса'
    )
    priority = models.SmallIntegerField(
        choices=PRIORITY_CHOICES,
        verbose_name='Приоритет запроса'
    )
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)





